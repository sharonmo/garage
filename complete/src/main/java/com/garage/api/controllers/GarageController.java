package com.garage.api.controllers;

import com.garage.api.requests.InflateTiresRequest;
import com.garage.api.requests.RechargeRequest;
import com.garage.api.requests.RefuelRequest;
import com.garage.api.requests.UpdateStatusRequest;
import com.garage.services.GarageService;
import com.garage.entities.enums.Status;
import com.garage.entities.vehicles.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("garage/")
public class GarageController {

    private GarageService garageService;

    @Autowired
    public GarageController(GarageService garageService) {
        this.garageService = garageService;
    }

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @PostMapping("/acceptBike")
    public void acceptBike(@RequestBody Bike bikeToAccept, @RequestParam(defaultValue = "false") boolean firstInLine) {
        garageService.acceptVehicleToGarage(bikeToAccept, firstInLine);
    }

    @PostMapping("/acceptCar")
    public void acceptCar(@RequestBody Car carToAccept, @RequestParam(defaultValue = "false") boolean firstInLine) {
        garageService.acceptVehicleToGarage(carToAccept, firstInLine);
    }

    @PostMapping("/acceptElectricCar")
    public void acceptElectricCar(@RequestBody ElectricCar electricCarToAccept, @RequestParam(defaultValue = "false") boolean firstInLine) {
        garageService.acceptVehicleToGarage(electricCarToAccept, firstInLine);
    }

    @PostMapping("/acceptTruck")
    public void acceptTruck(@RequestBody Truck truckToAccept, @RequestParam(defaultValue = "false") boolean firstInLine) {
        garageService.acceptVehicleToGarage(truckToAccept, firstInLine);
    }

    @DeleteMapping("/")
    public void removeVehicle(@RequestParam String licenseNumber) {
        garageService.removeVehicle(licenseNumber);
    }

    @GetMapping("/getNth")
    public Vehicle getNthVehicleFromTheEnd(@RequestParam int n) {
        return garageService.getNthVehicleFromTheEnd(n);
    }

    @GetMapping("/getAll")
    public List<Vehicle> getAll(@RequestParam(required = false) Status status) {
        return garageService.getAllVehicles(status);
    }

    @PostMapping("/updateStatus")
    public void updateVehicleStatus(@RequestParam String licenseNumber, @RequestBody UpdateStatusRequest request) {
        garageService.updateVehicleStatue(licenseNumber, request.getStatus());
    }

    @PostMapping("/inflateTires")
    public void inflateTires(@RequestParam String licenseNumber, @RequestBody InflateTiresRequest request) {
        garageService.inflateTires(licenseNumber, request.getDesiredPressure());
    }

    @PostMapping("/refuel")
    public void refuel(@RequestParam String licenseNumber, @RequestBody RefuelRequest request) {
        garageService.refuel(licenseNumber, request.getFuelType(), request.getFuelAmount());
    }

    @PostMapping("/recharge")
    public void recharge(@RequestParam String licenseNumber, @RequestBody RechargeRequest request) {
        garageService.recharge(licenseNumber, request.getInput());
    }

    @GetMapping("/getVehicleDetails")
    public String getVehicleDetails(@RequestParam String licenseNumber) {
        return garageService.getVehicleDetails(licenseNumber);
    }

    @GetMapping("/detectLoop")
    public boolean detectLoop() {
        return garageService.detectLoop();
    }

    @PatchMapping("/removeLoop")
    public void removeLoop() {
        garageService.removeLoop();
    }
}
