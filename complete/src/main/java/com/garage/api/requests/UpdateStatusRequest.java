package com.garage.api.requests;

import com.garage.entities.enums.Status;

public class UpdateStatusRequest {
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
