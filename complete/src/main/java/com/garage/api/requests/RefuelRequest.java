package com.garage.api.requests;

import com.garage.entities.enums.FuelType;

public class RefuelRequest {
    private FuelType fuelType;
    private int fuelAmount;

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public int getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(int fuelAmount) {
        this.fuelAmount = fuelAmount;
    }
}
