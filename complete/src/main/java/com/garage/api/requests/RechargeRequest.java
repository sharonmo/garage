package com.garage.api.requests;

public class RechargeRequest {
    private float input;

    public float getInput() {
        return input;
    }

    public void setInput(float input) {
        this.input = input;
    }
}
