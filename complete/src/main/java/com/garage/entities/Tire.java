package com.garage.entities;

public class Tire {
    private String makerName;
    private float currentPressure;
    private float maxPressure;

    public Tire() {
    }

    public Tire(float maxPressure) {
        this.maxPressure = maxPressure;
    }

    public void inflate (float desiredPressure){
        currentPressure = desiredPressure < maxPressure ? desiredPressure : maxPressure;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public float getCurrentPressure() {
        return currentPressure;
    }

    @Override
    public String toString() {
        return "Tire{" +
                "\rmakerName='" + makerName + '\'' +
                ", \rcurrentPressure=" + currentPressure +
                ", \rmaxPressure=" + maxPressure +
                "\r}";
    }
}
