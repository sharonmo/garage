package com.garage.entities.vehicles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.garage.entities.Tire;
import com.garage.entities.enums.Status;
import com.garage.linked.list.Node;
import org.springframework.lang.NonNull;

import java.util.List;

public abstract class Vehicle extends Node<Vehicle> {

    protected String makerName;

    protected String modelName;

    @JsonProperty(required = true)
    protected String licenseNumber;

    @JsonProperty(required = true)
    protected String owner;

    protected Status status = Status.AwaitingRepair;

    protected List<Tire> tires;

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public abstract float getAvailableEnergy();

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Tire> getTires() {
        return tires;
    }

    @Override
    public String getUniqueId() {
        return licenseNumber;
    }

    @Override
    @JsonIgnore
    public Vehicle getData() {
        return this;
    }
}
