package com.garage.entities.vehicles;

import com.garage.entities.Tire;

import java.util.ArrayList;

import static com.garage.entities.enums.FuelType.Diesel;

public class Truck extends FuelDrivenVehicle {
    private final int numOfDoors = 2;
    private boolean isCarryingDangerousMaterials;

    protected Truck() {
        super(Diesel, 140);
        this.tires = new ArrayList<Tire>(10){{
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
            add(new Tire(18));
        }};
    }

    public int getNumOfDoors() {
        return numOfDoors;
    }

    public boolean isCarryingDangerousMaterials() {
        return isCarryingDangerousMaterials;
    }

    public void setCarryingDangerousMaterials(boolean carryingDangerousMaterials) {
        isCarryingDangerousMaterials = carryingDangerousMaterials;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "\rfuelType=" + fuelType +
                ", \rcurrentFuelAmount=" + currentFuelAmount +
                ", \rmaxFuelAmount=" + maxFuelAmount +
                ", \rnumOfDoors=" + numOfDoors +
                ", \risCarryingDangerousMaterials=" + isCarryingDangerousMaterials +
                ", \rmakerName='" + makerName + '\'' +
                ", \rmodelName='" + modelName + '\'' +
                ", \rlicenseNumber='" + licenseNumber + '\'' +
                ", \rowner='" + owner + '\'' +
                ", \rstatus=" + status +
                ", \rtires=" + tires +
                "\r} ";
    }
}
