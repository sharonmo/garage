package com.garage.entities.vehicles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.garage.entities.enums.FuelType;

public abstract class FuelDrivenVehicle extends Vehicle {
    protected final FuelType fuelType;
    protected float currentFuelAmount;
    protected final float maxFuelAmount;

    protected FuelDrivenVehicle(FuelType fuelType, float maxFuelAmount) {
        this.fuelType = fuelType;
        this.maxFuelAmount = maxFuelAmount;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setCurrentFuelAmount(float currentFuelAmount) {
        if (currentFuelAmount < maxFuelAmount) {
            this.currentFuelAmount = currentFuelAmount;
        } else {
            this.currentFuelAmount = maxFuelAmount;
        }
    }

    public float getCurrentFuelAmount() {
        return currentFuelAmount;
    }

    public boolean refuel(FuelType fuelType, float amount) {
        if (fuelType != this.fuelType) {
            return false;
        }
        if (currentFuelAmount + amount < maxFuelAmount) {
            currentFuelAmount = currentFuelAmount + amount;
        } else {
            currentFuelAmount = maxFuelAmount;
        }
        return true;
    }

    @Override
    public float getAvailableEnergy() {
        if (maxFuelAmount == 0){
            return 0;
        }
        return currentFuelAmount / maxFuelAmount;
    }
}
