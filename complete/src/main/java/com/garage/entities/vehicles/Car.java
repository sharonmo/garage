package com.garage.entities.vehicles;

import com.garage.entities.Tire;

import java.util.ArrayList;

import static com.garage.entities.enums.FuelType.O95;

public class Car extends FuelDrivenVehicle {
    public enum Color {
        Red, Yellow, Black, White
    }

    public Car() {
        super(O95, 45);
        this.tires = new ArrayList<Tire>(4){{
            add(new Tire(27));
            add(new Tire(27));
            add(new Tire(27));
            add(new Tire(27));
        }};
    }

    private Color carColor;
    private int numOfDoors;

    public Color getCarColor() {
        return carColor;
    }

    public void setCarColor(Color carColor) {
        this.carColor = carColor;
    }

    public int getNumOfDoors() {
        return numOfDoors;
    }

    public void setNumOfDoors(int numOfDoors) {
        this.numOfDoors = numOfDoors;
    }

    @Override
    public String toString() {
        return "Car{" +
                "\ncarColor=" + carColor +
                ", \rnumOfDoors=" + numOfDoors +
                ", \rfuelType=" + fuelType +
                ", \rcurrentFuelAmount=" + currentFuelAmount +
                ", \rmaxFuelAmount=" + maxFuelAmount +
                ", \rmakerName='" + makerName + '\'' +
                ", \rmodelName='" + modelName + '\'' +
                ", \rlicenseNumber='" + licenseNumber + '\'' +
                ", \rowner='" + owner + '\'' +
                ", \rstatus=" + status +
                ", \rtires=" + tires +
                "\r} ";
    }
}
