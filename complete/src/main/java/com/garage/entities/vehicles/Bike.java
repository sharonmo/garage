package com.garage.entities.vehicles;

import com.garage.entities.Tire;
import com.garage.entities.enums.Status;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static com.garage.entities.enums.FuelType.O98;

public class Bike extends FuelDrivenVehicle {

    protected Bike() {
        super(O98, 5);
        this.tires = new ArrayList<Tire>(2){{
            add(new Tire(30));
            add(new Tire(30));
        }}.stream().limit(2).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Bike{" +
                "\rfuelType=" + fuelType +
                ", \rcurrentFuelAmount=" + currentFuelAmount +
                ", \rmaxFuelAmount=" + maxFuelAmount +
                ", \rmakerName='" + makerName + '\'' +
                ", \rmodelName='" + modelName + '\'' +
                ", \rlicenseNumber='" + licenseNumber + '\'' +
                ", \rowner='" + owner + '\'' +
                ", \rstatus=" + status +
                ", \rtires=" + tires +
                "\r} ";
    }

    public static final class BikeBuilder {
        protected float currentFuelAmount;
        protected String makerName;
        protected String modelName;
        protected String licenseNumber;
        protected String owner;
        protected float availableEnergy;
        protected Status status;

        private BikeBuilder() {
        }

        public static BikeBuilder aBike() {
            return new BikeBuilder();
        }

        public BikeBuilder withCurrentFuelAmount(float currentFuelAmount) {
            this.currentFuelAmount = currentFuelAmount;
            return this;
        }

        public BikeBuilder withMakerName(String makerName) {
            this.makerName = makerName;
            return this;
        }

        public BikeBuilder withModelName(String modelName) {
            this.modelName = modelName;
            return this;
        }

        public BikeBuilder withLicenseNumber(String licenseNumber) {
            this.licenseNumber = licenseNumber;
            return this;
        }

        public BikeBuilder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public BikeBuilder withAvailableEnergy(float availableEnergy) {
            this.availableEnergy = availableEnergy;
            return this;
        }

        public BikeBuilder withStatus(Status status) {
            this.status = status;
            return this;
        }

        public Bike build() {
            Bike bike = new Bike();
            bike.setMakerName(makerName);
            bike.setModelName(modelName);
            bike.setLicenseNumber(licenseNumber);
            bike.setOwner(owner);
            bike.setStatus(status);
            bike.currentFuelAmount = this.currentFuelAmount;
            return bike;
        }
    }
}
