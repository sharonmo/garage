package com.garage.entities.vehicles;

import com.garage.entities.Tire;

import java.util.ArrayList;

public class ElectricCar extends Vehicle {
    private float currentBatteryRunTime;
    private final float maxBatteryRunTime = 4;

    public ElectricCar() {
        this.tires = new ArrayList<Tire>(4){{
            add(new Tire(27));
            add(new Tire(27));
            add(new Tire(27));
            add(new Tire(27));
        }};
    }

    public void charge (float input) {
        currentBatteryRunTime = input < maxBatteryRunTime ? input : maxBatteryRunTime;
    }

    @Override
    public float getAvailableEnergy() {
        return currentBatteryRunTime / maxBatteryRunTime;
    }

    @Override
    public String toString() {
        return "ElectricCar{" +
                "\rcurrentBatteryRunTime=" + currentBatteryRunTime +
                ", \rmaxBatteryRunTime=" + maxBatteryRunTime +
                ", \rmakerName='" + makerName + '\'' +
                ", \rmodelName='" + modelName + '\'' +
                ", \rlicenseNumber='" + licenseNumber + '\'' +
                ", \rowner='" + owner + '\'' +
                ", \rstatus=" + status +
                ", \rtires=" + tires +
                "\r} ";
    }
}
