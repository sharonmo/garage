package com.garage.entities.enums;

public enum FuelType {
    O95, O96, O98, Diesel
}
