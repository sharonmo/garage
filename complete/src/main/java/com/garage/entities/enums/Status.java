package com.garage.entities.enums;

public enum Status {
    AwaitingRepair, Repaired, PayedForRepair
}
