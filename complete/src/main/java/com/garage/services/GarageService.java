package com.garage.services;

import com.garage.entities.Tire;
import com.garage.entities.enums.FuelType;
import com.garage.entities.enums.Status;
import com.garage.entities.vehicles.ElectricCar;
import com.garage.entities.vehicles.FuelDrivenVehicle;
import com.garage.entities.vehicles.Vehicle;
import com.garage.linked.list.LinkedList;
import com.garage.linked.list.Node;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GarageService {
    private LinkedList<Vehicle> vehicleLinkedList = new LinkedList<Vehicle>();

    public void acceptVehicleToGarage(Vehicle vehicle, boolean isFirstInLine) {
        if (isFirstInLine) {
            vehicleLinkedList.insertToStart(vehicle);
        } else {
            vehicleLinkedList.insertToEnd(vehicle);
        }
    }

    public void removeVehicle(String licenseNumber) {
        vehicleLinkedList.removeNode(licenseNumber);
    }

    public Vehicle getNthVehicleFromTheEnd(int n) {
        Node<Vehicle> node = vehicleLinkedList.getNthNodeFromTheEnd(n);
        return node != null ? node.getData() : null;
    }

    public List<Vehicle> getAllVehicles(Status statusToFilter) {
        List<Vehicle> vehiclesToReturn = new ArrayList<>();
        Node<Vehicle> head = vehicleLinkedList.getHead();
        if (head != null) {
            Node<Vehicle> current = head;
            while (current != null) {
                Vehicle currentVehicle = current.getData();
                if (statusToFilter == null) {
                    // If no Status to filter by - returning all vehicles
                    vehiclesToReturn.add(currentVehicle);
                } else if (statusToFilter.equals(currentVehicle.getStatus())) {
                    vehiclesToReturn.add(currentVehicle);
                }
                current = current.getNext();
            }
        }
        return vehiclesToReturn;
    }

    public void updateVehicleStatue(String licenseNumber, Status status) {
        Vehicle vehicle = getByLicenseNum(licenseNumber);
        if (vehicle != null) {
            vehicle.setStatus(status);
        }
    }

    public void inflateTires(String licenseNumber, float desiredPressure) {
        Vehicle vehicle = getByLicenseNum(licenseNumber);
        if (vehicle != null) {
            List<Tire> tires = vehicle.getTires();
            if (tires != null) {
                tires.stream().forEach((tire -> {
                    tire.inflate(desiredPressure);
                }));
            }
        }
    }

    public void refuel(String licenseNumber, FuelType fuelType, int fuelAmount) {
        Vehicle vehicle = getByLicenseNum(licenseNumber);
        if (vehicle != null) {
            if (vehicle instanceof FuelDrivenVehicle) {
                ((FuelDrivenVehicle) vehicle).refuel(fuelType, fuelAmount);
            }
        }
    }

    public void recharge(String licenseNumber, float input) {
        Vehicle vehicle = getByLicenseNum(licenseNumber);
        if (vehicle != null) {
            if (vehicle instanceof ElectricCar) {
                ((ElectricCar) vehicle).charge(input);
            }
        }
    }

    public String getVehicleDetails(String licenseNumber) {
        Vehicle vehicle = getByLicenseNum(licenseNumber);
        return vehicle != null ? vehicle.toString() : "";
    }

    public boolean detectLoop() {
        return vehicleLinkedList.detectLoop();
    }

    public void removeLoop() {
        vehicleLinkedList.removeLoop();
    }

    private Vehicle getByLicenseNum(String licenseNumber) {
        Node<Vehicle> head = vehicleLinkedList.getHead();
        if (head != null) {
            Node<Vehicle> current = head;
            while (current != null) {
                Vehicle currentVehicle = current.getData();
                if (licenseNumber.equals(currentVehicle.getLicenseNumber())) {
                    return currentVehicle;
                }
                current = current.getNext();
            }
        }
        return null;
    }
}
