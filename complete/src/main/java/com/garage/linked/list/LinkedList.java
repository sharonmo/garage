package com.garage.linked.list;

import java.util.HashSet;
import java.util.Set;

public class LinkedList<T> {
    private Node<T> head;

    public Node<T> getHead() {
        return head;
    }

    public void insertToEnd(Node n) {
        if (head == null) {
            head = n;
        } else {
            Node last = head;
            while (last.getNext() != null) {
                last = last.getNext();
            }
            last.setNext(n);
        }
    }

    public void insertToStart(Node n) {
        if (head == null) {
            head = n;
        } else {
            n.setNext(head);
            head = n;
        }
    }

    public void removeNode(String idToRemove) {
        if (head != null) {
            Node current = head, prev = null;
            if (current != null && current.getUniqueId().equals(idToRemove)) {
                head = current.getNext();
                return;
            }
            while (current != null && !current.getUniqueId().equals(idToRemove)) {
                prev = current;
                current = current.getNext();
            }
            if (current == null) {
                return;
            } else {
                prev.setNext(current.getNext());
            }
        }
    }

    public Node getNthNodeFromTheEnd(int n) {
        int length = 0;
        Node current = head;

        while (current != null) {
            current = current.getNext();
            length++;
        }
        if (length < n){
            return null;
        } else {
            current = head;
            for (int i = 1; i < length - n + 1; i++) {
                current = current.getNext();
            }
            return current;
        }
    }

    public boolean detectLoop (){
        Set<Node> set = new HashSet<>();
        Node current = head;
        while (current != null){
            if (set.contains(current)){
                return true;
            }
            set.add(current);
            current = current.getNext();
        }
        return false;
    }

    public void removeLoop (){
        Set<Node> set = new HashSet<>();
        Node current = head, previous = null;
        while (current != null){
            if (set.contains(current)){
                previous.setNext(null);
                return;
            }
            set.add(current);
            previous = current;
            current = current.getNext();
        }
    }
}
