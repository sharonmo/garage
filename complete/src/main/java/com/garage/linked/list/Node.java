package com.garage.linked.list;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Node <T> {
    private Node next;

    @JsonIgnore
    public abstract T getData();

    public Node() {
        next = null;
    }

    @JsonIgnore
    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public abstract String getUniqueId ();
}
